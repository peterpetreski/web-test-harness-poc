module.exports = {
//   rootURI: process.env.WEB_ROOT_URL ? process.env.WEB_ROOT_URL : 'http://localhost:8081/',
  // rootURI: process.env.WEB_ROOT_URL ? process.env.WEB_ROOT_URL : "https://portal.staging.scalapay.com/",
  rootURI: process.env.WEB_ROOT_URL ? process.env.WEB_ROOT_URL : 'https://portal.development.scalapay.com/',
  loginSlug: process.env.WEB_LOGIN_SLUG ? process.env.WEB_LOGIN_SLUG : 'login',
  adminUsername: process.env.WEB_USERNAME ? process.env.WEB_USERNAME : 'test@scalapay.com',
  adminPassword: process.env.WEB_PASSWORD ? process.env.WEB_PASSWORD : '12345678',
//   apiURL: 'http://localhost:8080/v2/',
  // apiURL: "https://staging.api.scalapay.com/v2/",
  apiURL: 'https://development.api.scalapay.com/v2/',
  configurationsEndpoint: 'configurations',
  ordersEndpoint: 'orders',
  paymentsEndpoint: 'payments',
  captureEndpoint: 'capture',
  dashboardEndpoint: 'account/dashboard',
  authToken: 'qhtfs87hjnc12kkos',
  authScheme: 'Bearer',
  countryCode: '+61',
  smsToolDevelopmentEndpoint: 'https://sms.development.scalapay.com/v1/destination',
  smsToolStagingEndpoint: 'https://sms.staging.scalapay.com/v1/destination',
  smsToolProductionEndpoint: 'https://sms.scalapay.com/v1/destination',
};

// jest.config.js
module.exports = async () => ({
  verbose: true,
  preset: 'jest-puppeteer',
  rootDir: './',
  setupFiles: ['dotenv/config']
});


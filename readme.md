## Basic test harness for scalapay web interface using nodejs/puppeteer/jest
## Install
After cloning the repo, run `npm install`


## Running tests
To run the tests do the following:
`npm test`

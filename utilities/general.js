/* eslint-disable no-undef */
/* eslint-disable sonarjs/no-duplicate-string */
/* eslint-disable class-methods-use-this */
require('dotenv').config();
const dotenv = require('dotenv');

dotenv.load({ path: '.env' });

const loginHelper = require('./login-helpers');
const ApiHelper = require('./api/api-request-main');
const configs = require('../env.config');

const apiReq = new ApiHelper();

/**
 * General Helper methods
 */
class ScalaGeneralHelpers {
  /**
     * Generates a complete basic order flow
     * @returns orderToken
     */
  async generateCompleteOrder() {
    try {
      let endpoint = configs.ordersEndpoint;

      // Initiate order via API
      let testParams = {
        requestType: 'post',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `${configs.authScheme} ${configs.authToken}`
        },
        endpoint,
      };
      const data = JSON.stringify({
        totalAmount: {
          amount: '100.00', currency: 'EUR'
        },
        consumer: {
          phoneNumber: '0400000001', givenNames: 'Joe', surname: 'Consumer', email: 'test-user@scalapay.com'
        },
        billing: {
          name: 'Joe Consumer', line1: 'Via della Rosa, 58', suburb: 'Montelupo Fiorentino', postcode: '50056', countryCode: 'IT', phoneNumber: '0400000000'
        },
        shipping: {
          name: 'Joe Consumer', line1: 'Via della Rosa, 58', suburb: 'Montelupo Fiorentino', postcode: '50056', countryCode: 'IT', phoneNumber: '0400000000'
        },
        items: [
          {
            name: '32', category: 'clothes', subcategory: ['shirt', 'long-sleeve'], brand: 'TopChoice', gtin: '123458791330', sku: '12341234', quantity: 1, price: { amount: '10.00', currency: 'EUR' }
          },
          {
            name: 'Jeans', category: 'clothes', subcategory: ['pants', 'jeans'], brand: 'TopChoice', gtin: '123458722222', sku: '12341235', quantity: 1, price: { amount: '20.00', currency: 'EUR' }
          }
        ],
        discounts: [
          {
            displayName: '10% Off', amount: { amount: '103.00', currency: 'EUR' }
          }
        ],
        merchant: {
          redirectConfirmUrl: `${configs.rootURI}account/dashboard`, redirectCancelUrl: `${configs.rootURI}failure-url`
        },
        merchantReference: 'merchantOrder-1234',
        taxAmount: {
          amount: '3.70', currency: 'EUR'
        },
        shippingAmount: {
          amount: '10.00', currency: 'EUR'
        },
        orderExpiryMilliseconds: 6000000
      });

      const scalaResponse = await apiReq.scalaAPIRequest(testParams, data);

      const checkoutURL = scalaResponse.data.checkoutUrl;
      const orderToken = scalaResponse.data.token;
      await page.goto(checkoutURL);
      let pageTitle = await page.title();

      // Workaround for development environment - need to use _login slug
      if (page.url().includes('https://portal.development.scalapay.com/login')) {
        const devLoginURL = page.url();
        const newLoginURL = devLoginURL.replace('login', '_login');
        await page.goto(newLoginURL);
      }

      if (/(Registrati|Register)/.test(pageTitle)) {
        const loginLink = await page.$('a[href="/login"]');

        await loginLink.click();
        await page.waitForNavigation({ waitUntil: 'networkidle2' });
      }

      // Should be at login page now
      pageTitle = await page.title();

      await loginHelper.enterLoginDetails();
      await loginHelper.clickLoginSubmit();

      await page.waitForNavigation({ waitUntil: 'networkidle2' });

      const myButton = await page.$('#checkout-continue-button');
      await myButton.click();

      // workaround for localhost environment - ie, redirects to "https" will fail so we need to intercept and change back to "http"
      await page.setRequestInterception(true);
      page.on('request', (interceptedRequest) => {
        // Intercept if request url starts with https
        if (interceptedRequest.url().startsWith('https://localhost')) {
          interceptedRequest.continue({
            // Replace https:// in url with http://
            url: interceptedRequest.url().replace('https://', 'http://'),
          });
          return;
        }

        // Don't override other requests
        interceptedRequest.continue();
      });

      await page.waitForSelector('input[id="searchField"]', { visible: true, timeout: 0 });
      pageTitle = await page.title();

      // Now do a capture via the API using the order token string
      const data2 = JSON.stringify({ token: orderToken, merchantReference: 'merchantOrder-1234' });
      endpoint = `${configs.paymentsEndpoint}/${configs.captureEndpoint}`;
      testParams = {
        requestType: 'post',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `${configs.authScheme} ${configs.authToken}`
        },
        endpoint,
      };
      const captureResponse = await apiReq.scalaAPIRequest(testParams, data2);
      if (captureResponse.status === 200) {
        return orderToken;
      }
      return false;
    } catch (error) {
      console.log('Error - generateCompleteOrder:', error);
    }
  }
}
module.exports = new ScalaGeneralHelpers();

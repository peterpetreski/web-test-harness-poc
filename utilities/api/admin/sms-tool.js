/* eslint-disable class-methods-use-this */
require('dotenv').config();
const dotenv = require('dotenv');

dotenv.load({ path: '.env' });

const ApiHelper = require('../api-request-main');
const configs = require('../../../env.config');

const apiReq = new ApiHelper();

/**
 * Helper methods for the SMS API tool
 * WIP - need a way to figure out which environment the test harness is running in.
 * This is because the SMS API URLs endpoints are different for each environment
 */
class SMSHelpers {
  /**
   * Returns the last SMS code sent to the specified number
   * @param {*} phoneNumber (must be in format +<CC><number without leading zero>)
   * @returns SMS code, FALSE (on failure)
   */
  async getSMSCode(phoneNumber = '') {
    try {
      if (!phoneNumber) {
        console.log('Error - getSMSCode: phoneNumber missing');
        return false;
      }
      const currentEnv = process.env.ENVIRONMENT ? process.env.ENVIRONMENT : 'development';
      let endpoint = '';
      let apiKey = '';
      if (currentEnv === 'development') {
        endpoint = `${configs.smsToolDevelopmentEndpoint}/${phoneNumber}`;
        apiKey = 'dcv6ib5kQ58F6xvuFYbPo3gQoxCQtIcL2EUMsH7l';
      } else if (currentEnv === 'staging') {
        endpoint = `${configs.smsToolStagingEndpoint}/${phoneNumber}`;
        apiKey = 'nPwkMEasG86SgN7jtD8Gf98B6kr1JRcz57gfvH30';
      } else if (currentEnv === 'production') {
        endpoint = `${configs.smsToolProductionEndpoint}/${phoneNumber}`;
        apiKey = 'OO87YrpMtK7Q6EDeqHxyv3KsXWWxVgJV9sV2Gsqw';
      } else {
        // TODO - which endpoint do you use for localhost testing?
      }

      // Get the SMS code via API
      const testParams = {
        requestType: 'get',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'x-api-key': apiKey
        },
        endpoint,
      };

      // TODO - Currently the sms api tool is returning an invalid JSON object, ie, extra {} on the top. (Mark looking at)
      return await apiReq.smsToolAPIRequest(testParams); // TODO - get the last SMS code in the list returned from the API
    } catch (error) {
      console.log('Error - getPaymentDetails:', error);
    }
  }
}

module.exports = new SMSHelpers();

/* eslint-disable no-prototype-builtins */
/* eslint-disable class-methods-use-this */
const axios = require('axios');
// const testConfig = require('../config.json');
const testConfig = require('../../env.config');

module.exports = class scalaAPICalls {
  /**
       * Sends HTTP request to Scala API URL endpoints
       * Will set defaults where applicable or where params not included
       * By default it will send requests to public API + endpoint:
       * eg: https://staging.api.scalapay.com/v2/<endpoint>
       *
       *
       * @param {*} reqParams: object containing request parameters for the HTTP message, eg:
       *    {
       *       requestType: 'post/get/put/delete',
       *       headers: {},
       *       endpoint: 'xyz',
       *       queryParams: qParams
       *     };
       *
       * @param {*} data: object containing the data you want to submit to endpoint
       */

  async scalaAPIRequest(reqParams = {}, data = {}) {
    try {
      if (!Object.keys(reqParams).length) {
        throw new Error('Please provide a valid reqParams object');
      }

      const axiosRequestObject = {};

      // set requestType
      if (reqParams.hasOwnProperty('requestType') && reqParams.requestType !== '') {
        axiosRequestObject.method = reqParams.requestType;
      } else {
        axiosRequestObject.method = 'get'; // set default
      }

      // set URL
      if (reqParams.hasOwnProperty('endpoint') && reqParams.endpoint !== '') {
        axiosRequestObject.url = `${testConfig.apiURL}${reqParams.endpoint}`;
      } else {
        axiosRequestObject.url = `${testConfig.apiURL}`; // set default
      }

      axiosRequestObject.data = data;

      // set headers
      if (reqParams.hasOwnProperty('headers') && Object.keys(reqParams.headers).length > 0) {
        axiosRequestObject.headers = reqParams.headers;
      } else {
        axiosRequestObject.headers = { Authorization: `Bearer ${testConfig.authToken}` }; // set default
      }

      return await axios(axiosRequestObject);
    } catch (error) {
      return error;
    }
  }

  async smsToolAPIRequest(reqParams = {}, data = {}) {
    try {
      if (!Object.keys(reqParams).length) {
        throw new Error('Please provide a valid reqParams object');
      }

      const axiosRequestObject = {};

      // set requestType
      if (reqParams.hasOwnProperty('requestType') && reqParams.requestType !== '') {
        axiosRequestObject.method = reqParams.requestType;
      } else {
        axiosRequestObject.method = 'get'; // set default
      }

      // set URL
      if (reqParams.hasOwnProperty('endpoint') && reqParams.endpoint !== '') {
        axiosRequestObject.url = `${reqParams.endpoint}`;
      } else {
        console.log('Error - smsToolAPIRequest: endpoint missing');
        return false;
      }

      axiosRequestObject.data = data;

      // set headers
      if (reqParams.hasOwnProperty('headers') && Object.keys(reqParams.headers).length > 0) {
        axiosRequestObject.headers = reqParams.headers;
      } else {
        console.log('Error - smsToolAPIRequest: request headers missing');
        return false;
      }

      return await axios(axiosRequestObject);
    } catch (error) {
      return error;
    }
  }
};

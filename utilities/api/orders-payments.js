/* eslint-disable class-methods-use-this */
require('dotenv').config();
const dotenv = require('dotenv');

dotenv.load({ path: '.env' });

const ApiHelper = require('./api-request-main');
const configs = require('../../env.config');

const apiReq = new ApiHelper();

/**
 * Helper methods for orders and payments API tasks
 */
class OrdersAndPayments {
  /**
     * Initiates an order (POST)
     * Accepts data object or sets a default one if not provided
     * @param {*} data
     * @returns Object (API response)
     */
  async createOrder(data = {}) {
    try {
      const endpoint = configs.ordersEndpoint;

      // Initiate order via API
      const testParams = {
        requestType: 'post',
        headers: {
          // eslint-disable-next-line sonarjs/no-duplicate-string
          Accept: 'application/json',
          // eslint-disable-next-line sonarjs/no-duplicate-string
          'Content-Type': 'application/json',
          Authorization: `${configs.authScheme} ${configs.authToken}`
        },
        endpoint,
      };
      if (Object.keys(data).length === 0) {
        data = JSON.stringify({
          totalAmount: {
            amount: '100.00', currency: 'EUR'
          },
          consumer: {
            phoneNumber: '0400000001', givenNames: 'Joe', surname: 'Consumer', email: 'test-user@scalapay.com'
          },
          billing: {
            name: 'Joe Consumer', line1: 'Via della Rosa, 58', suburb: 'Montelupo Fiorentino', postcode: '50056', countryCode: 'IT', phoneNumber: '0400000000'
          },
          shipping: {
            name: 'Joe Consumer', line1: 'Via della Rosa, 58', suburb: 'Montelupo Fiorentino', postcode: '50056', countryCode: 'IT', phoneNumber: '0400000000'
          },
          items: [
            {
              name: '32', category: 'clothes', subcategory: ['shirt', 'long-sleeve'], brand: 'TopChoice', gtin: '123458791330', sku: '12341234', quantity: 1, price: { amount: '10.00', currency: 'EUR' }
            },
            {
              name: 'Jeans', category: 'clothes', subcategory: ['pants', 'jeans'], brand: 'TopChoice', gtin: '123458722222', sku: '12341235', quantity: 1, price: { amount: '20.00', currency: 'EUR' }
            }
          ],
          discounts: [
            {
              displayName: '10% Off', amount: { amount: '103.00', currency: 'EUR' }
            }
          ],
          merchant: {
            redirectConfirmUrl: `${configs.rootURI}account/dashboard`, redirectCancelUrl: `${configs.rootURI}failure-url`
          },
          merchantReference: 'merchantOrder-1234',
          taxAmount: {
            amount: '3.70', currency: 'EUR'
          },
          shippingAmount: {
            amount: '10.00', currency: 'EUR'
          },
          orderExpiryMilliseconds: 6000000
        });
      }

      return await apiReq.scalaAPIRequest(testParams, data);
    } catch (error) {
      console.log('Error - createOrder:', error);
    }
  }

  /**
     * Updates the merchant reference of an order (POST)
     * @param {*} orderToken
     * @param {*} data
     * @returns API response object on success or FALSE on failure
     */
  async updateOrderMerchantRef(orderToken = '', data = {}) {
    try {
      if (!orderToken) {
        console.log('Error - updateOrderMerchantRef: orderToken missing');
        return false;
      }
      const endpoint = `${configs.ordersEndpoint}/${orderToken}`;

      // Initiate order via API
      const testParams = {
        requestType: 'post',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `${configs.authScheme} ${configs.authToken}`
        },
        endpoint,
      };
      if (Object.keys(data).length === 0) {
        data = JSON.stringify({
          merchantReference: 'merchantOrder-1234-updated',
        });
      }

      return await apiReq.scalaAPIRequest(testParams, data);
    } catch (error) {
      console.log('Error - updateOrderMerchantRef:', error);
    }
  }

  /**
     * Performs a capture on an order (POST)
     * @param {*} orderToken
     * @param {*} merchantRef (optional)
     * @returns API response object on success or FALSE on failure
     */
  async paymentCapture(orderToken = '', merchantRef = '') {
    try {
      if (!orderToken) {
        console.log('Error - paymentCapture: orderToken missing');
        return false;
      }

      const endpoint = `${configs.paymentsEndpoint}/${configs.captureEndpoint}`;

      const testParams = {
        requestType: 'post',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `${configs.authScheme} ${configs.authToken}`
        },
        endpoint,
      };
      const data = JSON.stringify({
        token: orderToken,
        ...(merchantRef && { merchantReference: merchantRef }),
      });
      return await apiReq.scalaAPIRequest(testParams, data);
    } catch (error) {
      console.log('Error - paymentCapture:', error);
    }
  }

  /**
     * Retrieves payments details of an order (GET)
     * Takes the order token as a mandatory parameter
     * @param {*} orderToken
     * @returns API response object on success or FALSE on failure
     */
  async getPaymentDetails(orderToken = '') {
    try {
      if (!orderToken) {
        console.log('Error - getPaymentDetails: orderToken missing');
        return false;
      }
      const endpoint = `${configs.paymentsEndpoint}/${orderToken}`;

      // Initiate order via API
      const testParams = {
        requestType: 'get',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `${configs.authScheme} ${configs.authToken}`
        },
        endpoint,
      };

      return await apiReq.scalaAPIRequest(testParams);
    } catch (error) {
      console.log('Error - getPaymentDetails:', error);
    }
  }

  /**
     * Performs a refund (POST)
     * All Params except merchantRef are mandatory
     * @param {*} orderToken
     * @param {*} amount
     * @param {*} currency
     * @param {*} merchantRef
     * @returns API response object on success or FALSE on failure
     */
  async refundPayment(orderToken = '', amount = '', currency = '', merchantRef = '') {
    try {
      if (!orderToken) {
        console.log('Error - refundPayment: orderToken missing');
        return false;
      }

      if (!amount) {
        console.log('Error - refundPayment: refund amount missing');
        return false;
      }

      if (!currency) {
        console.log('Error - refundPayment: currency missing');
        return false;
      }

      const endpoint = `${configs.paymentsEndpoint}/${orderToken}/refund`;

      const testParams = {
        requestType: 'post',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `${configs.authScheme} ${configs.authToken}`
        },
        endpoint,
      };
      const data = JSON.stringify({
        refundAmount: {
          amount,
          currency
        },
        ...(merchantRef && { merchantReference: merchantRef }),
      });
      return await apiReq.scalaAPIRequest(testParams, data);
    } catch (error) {
      console.log('Error - refundPayment:', error);
    }
  }

  /**
     * Perform a delay capture
     * @param {*} orderToken
     * @param {*} authExpiry
     * @param {*} merchantRef (optional)
     * @returns API response object on success or FALSE on failure
     */
  async delayCapture(orderToken = '', authExpiry = '', merchantRef = '') {
    try {
      if (!orderToken) {
        console.log('Error - delayCapture: orderToken missing');
        return false;
      }

      if (!authExpiry) {
        console.log('Error - delayCapture: authExpiry missing');
        return false;
      }

      const endpoint = `${configs.paymentsEndpoint}/${orderToken}/delay`;

      const testParams = {
        requestType: 'post',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `${configs.authScheme} ${configs.authToken}`
        },
        endpoint,
      };
      const data = JSON.stringify({
        authorizationExpiryMilliseconds: authExpiry,
        ...(merchantRef && { merchantReference: merchantRef }),
      });
      return await apiReq.scalaAPIRequest(testParams, data);
    } catch (error) {
      console.log('Error - refundPayment:', error);
    }
  }
}

module.exports = new OrdersAndPayments();

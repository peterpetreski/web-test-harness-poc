/* eslint-disable no-empty-function */
/* eslint-disable no-return-assign */
/* eslint-disable sonarjs/no-duplicate-string */
/* eslint-disable no-undef */
/* eslint-disable class-methods-use-this */
const conf = require('../env.config');

/**
 * Helper methods for login page tasks
 */
class ScalaLogin {
  async gotoLoginPage(url = conf.rootURI + conf.loginSlug) {
    await page.goto(url);
    await page.waitForSelector('input[id=password]');
  }

  async enterLoginDetails(username = conf.adminUsername, password = conf.adminPassword) {
    // eslint-disable-next-line no-return-assign
    await page.$eval('input[id=email]', el => el.value = ''); // clear first just in case
    await page.type('input[id=email]', username);
    await page.type('input[id=password]', password);
  }

  async login(username = conf.adminUsername, password = conf.adminPassword, url = conf.rootURI + conf.loginSlug) {
    await this.gotoLoginPage(url);
    await page.$eval('input[id=email]', el => el.value = ''); // clear first just in case
    await page.type('input[id=email]', username);
    await page.type('input[id=password]', password);
    await page.click('button[id=scala-login]');
  }

  // eslint-disable-next-line sonarjs/no-identical-functions
  async gotoLoginAndSubmit(username = conf.adminUsername, password = conf.adminPassword, url = conf.rootURI + conf.loginSlug) {
    await this.gotoLoginPage(url);
    await page.$eval('input[id=email]', el => el.value = ''); // clear first just in case
    await page.type('input[id=email]', username);
    await page.type('input[id=password]', password);
    await page.click('button[id=scala-login]');
  }

  async clickLoginSubmit() {
    await page.click('button[id=scala-login]');
  }

  // TODO
  async logout() {
  }

  // TODO
  async isLoggedIn() {
  }
}
module.exports = new ScalaLogin();

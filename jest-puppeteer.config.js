module.exports = {
    launch: {
      headless: false,
      slowMo: 10,
      defaultViewport: null,
      args: ['--start-maximized', '--no-sandbox', '--disable-setuid-sandbox']
    },
    browser: 'chromium',
    browserContext: 'default',
    executablePath: '/usr/bin/chromium-browser'
}

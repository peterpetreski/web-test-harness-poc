/* eslint-disable sonarjs/no-duplicate-string */
/* eslint-disable no-undef */

require('dotenv').config();
const dotenv = require('dotenv');

dotenv.load({ path: '.env' });

const loginHelper = require('./utilities/login-helpers');
const ApiHelper = require('./utilities/api/api-request-main');
const configs = require('./env.config');
const ScalaGeneralHelpers = require('./utilities/general');
const orderAPIHelpers = require('./utilities/api/orders-payments');
const smsHelpers = require('./utilities/api/admin/sms-tool');


const models = require('@scalapay/scalapay-model/models');
const { expect } = require('@jest/globals');

const Invoice = models.invoice;
const Order = models.order;
const Transfer = models.transfer;

const testTimeout = 60000;

const apiReq = new ApiHelper();

describe('Dashboard - Orders page tests', () => {
  beforeAll(async () => {
  }, testTimeout);

  afterAll(async () => {
    models.sequelize.close();
  }, testTimeout);

  test('Verify successful order callflow using username/pass login', async () => {
    // Initiate order via API
    const scalaResponse = await orderAPIHelpers.createOrder();
    expect(scalaResponse.status).toEqual(200);

    const checkoutURL = scalaResponse.data.checkoutUrl;
    const orderToken = scalaResponse.data.token;

    // Query DB Order table
    const orderObject = await Order.findOne({
      where: {
        orderToken
      }
    });

    expect(orderObject.orderToken).toEqual(orderToken);
    expect(orderObject.status).toEqual('pending');

    await page.goto(checkoutURL);
    let pageTitle = await page.title();

    // Workaround for development environment - need to use _login slug
    if (page.url().includes('https://portal.development.scalapay.com/login')) {
      const devLoginURL = page.url();
      const newLoginURL = devLoginURL.replace('login', '_login');
      await page.goto(newLoginURL);
    }

    if (/(Registrati|Register)/.test(pageTitle)) {
      const loginLink = await page.$('a[href="/login"]');

      await loginLink.click();
      await page.waitForNavigation({ waitUntil: 'networkidle2' });
    }

    // Should be at login page now
    pageTitle = await page.title();
    expect(pageTitle).toMatch(/(Accedi|Log in)/);

    await loginHelper.enterLoginDetails();
    await loginHelper.clickLoginSubmit();

    await page.waitForNavigation({ waitUntil: 'networkidle2' });

    const myButton = await page.$('#checkout-continue-button');
    await myButton.click();

    // workaround for localhost environment - ie, redirects to "https" will fail so we need to intercept and change back to "http"
    await page.setRequestInterception(true);
    page.on('request', (interceptedRequest) => {
      // Intercept if request url starts with https
      if (interceptedRequest.url().startsWith('https://localhost')) {
        interceptedRequest.continue({
          // Replace https:// in url with http://
          url: interceptedRequest.url().replace('https://', 'http://'),
        });
        return;
      }

      // Don't override other requests
      interceptedRequest.continue();
    });

    await page.waitForSelector('input[id="searchField"]', { visible: true, timeout: 0 });
    pageTitle = await page.title();

    // Check "status" value of Order table in DB
    await orderObject.reload();
    expect(orderObject.status).toEqual('authorized');

    // Now do a capture via the API
    const captureResponse = await orderAPIHelpers.paymentCapture(orderToken);
    expect(captureResponse.status).toEqual(200);

    // Check "status" value of Order table in DB
    await orderObject.reload();
    expect(orderObject.status).toEqual('charged');


    // Check "status" value of transfer table in DB
    const transferObject = await Transfer.findOne({
      where: {
        orderId: orderObject.id
      }
    });

    expect(transferObject.status).toEqual('pending');

    // Verify there are 3 invoices
    const invoiceArray = await Invoice.findAll({
      where: {
        orderId: orderObject.id
      }
    });

    expect(invoiceArray.length).toEqual(3);

    // Reload page to display latest captured order
    await page.reload({ waitUntil: ['networkidle2', 'domcontentloaded'] });

    const orderPath = `/account/orders/${orderToken}`;
    const orderLink = await page.$(`a[href="${orderPath}"]`);

    await orderLink.click();
    await page.waitForNavigation({ waitUntil: 'networkidle2' });

    // TODO - ask developers to add a CSS ID or something unique to identify each table on the "Order" page
    const tbodyHandles = await page.$$('tbody');
    const rowHandles = await tbodyHandles[3].$$('tr');
    const tdHandlesFirstRow = await rowHandles[0].$$('td');
    const invoiceLink = await tdHandlesFirstRow[0].$(`a[href="/account/orders/${orderToken}/invoices/${orderToken}-1"]`);
    expect(invoiceLink).not.toBeNull();
  }, testTimeout);

  test('WIP - Verify successful order callflow using OTP login', async () => {
    const endpoint = configs.ordersEndpoint;

    // Initiate order via API
    const testParams = {
      requestType: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `${configs.authScheme} ${configs.authToken}`
      },
      endpoint,
    };
    const data = JSON.stringify({
      totalAmount: {
        amount: '100.00', currency: 'EUR'
      },
      consumer: {
        phoneNumber: '0400000001', givenNames: 'Joe', surname: 'Consumer', email: 'test-user@scalapay.com'
      },
      billing: {
        name: 'Joe Consumer', line1: 'Via della Rosa, 58', suburb: 'Montelupo Fiorentino', postcode: '50056', countryCode: 'IT', phoneNumber: '0400000000'
      },
      shipping: {
        name: 'Joe Consumer', line1: 'Via della Rosa, 58', suburb: 'Montelupo Fiorentino', postcode: '50056', countryCode: 'IT', phoneNumber: '0400000000'
      },
      items: [
        {
          name: '32', category: 'clothes', subcategory: ['shirt', 'long-sleeve'], brand: 'TopChoice', gtin: '123458791330', sku: '12341234', quantity: 1, price: { amount: '10.00', currency: 'EUR' }
        },
        {
          name: 'Jeans', category: 'clothes', subcategory: ['pants', 'jeans'], brand: 'TopChoice', gtin: '123458722222', sku: '12341235', quantity: 1, price: { amount: '20.00', currency: 'EUR' }
        }
      ],
      discounts: [
        {
          displayName: '10% Off', amount: { amount: '103.00', currency: 'EUR' }
        }
      ],
      merchant: {
        redirectConfirmUrl: `${configs.rootURI}account/dashboard`, redirectCancelUrl: `${configs.rootURI}failure-url`
      },
      merchantReference: 'merchantOrder-1234',
      taxAmount: {
        amount: '3.70', currency: 'EUR'
      },
      shippingAmount: {
        amount: '10.00', currency: 'EUR'
      },
      orderExpiryMilliseconds: 6000000
    });

    const scalaResponse = await apiReq.scalaAPIRequest(testParams, data);
    expect(scalaResponse.status).toEqual(200);

    const checkoutURL = scalaResponse.data.checkoutUrl;
    const orderToken = scalaResponse.data.token;

    // Query DB Order table
    const orderObject = await Order.findOne({
      where: {
        orderToken
      }
    });

    expect(orderObject.orderToken).toEqual(orderToken);
    expect(orderObject.status).toEqual('pending');

    await page.goto(checkoutURL);
    await page.waitForSelector('#scala-login', { visible: true, timeout: 0 });
    // await page.waitForNavigation({ waitUntil: "networkidle2" });

    const pageTitle = await page.title();
    expect(pageTitle).toMatch(/(Riepilogo di pagamento - Scalapay)/);

    // Click country code dropdown button
    const countryDropdown = await page.$('#countryCodeDropdownButton');
    await countryDropdown.click();

    await page.waitForSelector('.dropdown.show', { visible: true, timeout: 0 });

    // let countryItems = await page.$$()

    /*
    // workaround for localhost environment - ie, redirects to "https" will fail so we need to intercept and change back to "http"
    await page.setRequestInterception(true);
    page.on('request', (interceptedRequest) => {
      // Intercept if request url starts with https
      if (interceptedRequest.url().startsWith('https://localhost')) {
        interceptedRequest.continue({
          // Replace https:// in url with http://
          url: interceptedRequest.url().replace('https://', 'http://'),
        });
        return;
      }

      // Don't override other requests
      interceptedRequest.continue();
    });

    await page.waitForSelector('input[id="searchField"]', { visible: true, timeout: 0 });
    pageTitle = await page.title();

    // Check "status" value of Order table in DB
    await orderObject.reload();
    expect(orderObject.status).toEqual('authorized');

    // Now do a capture via the API using the order token string
    let data2 = JSON.stringify({ "token": orderToken, "merchantReference": "merchantOrder-1234" });
    endpoint = configs.paymentsEndpoint + '/' + configs.captureEndpoint;
    testParams = {
      requestType: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `${configs.authScheme} ${configs.authToken}`
      },
      endpoint: endpoint,
    };
    const captureResponse = await apiReq.scalaAPIRequest(testParams, data2);
    expect(captureResponse.status).toEqual(200);

    // Check "status" value of Order table in DB
    await orderObject.reload();
    expect(orderObject.status).toEqual('charged');


    // Check "status" value of transfer table in DB
    const transferObject = await Transfer.findOne({
      where: {
        orderId: orderObject.id
      }
    });

    expect(transferObject.status).toEqual('pending');

    // Verify there are 3 invoices
    const invoiceArray = await Invoice.findAll({
      where: {
        orderId: orderObject.id
      }
    });

    expect(invoiceArray.length).toEqual(3);

    // Reload page to display latest captured order
    await page.reload({ waitUntil: ["networkidle2", "domcontentloaded"] });

    let orderPath = `/account/orders/${orderToken}`;
    let orderLink = await page.$(`a[href="${orderPath}"]`);

    // await page.tracing.start({ categories: ['devtools.timeline'], path: "./tracing.json" });

    await orderLink.click();
    await page.waitForNavigation({ waitUntil: "networkidle2" });
    await page.screenshot({ path: "./test9999.png", fullPage: true });

    // TODO - ask developers to add a CSS ID or something unique to identify each table on the "Order" page
    let tbodyHandles = await page.$$('tbody');
    let rowHandles = await tbodyHandles[3].$$('tr');
    let tdHandlesFirstRow = await rowHandles[0].$$('td');

    // let xyz = await tdHandlesFirstRow[0].evaluate((node) => node.innerText);

    let invoiceLink = await tdHandlesFirstRow[0].$(`a[href="/account/orders/${orderToken}/invoices/${orderToken}-1"]`);
    expect(invoiceLink).not.toBeNull();
*/
  }, testTimeout);

  test.only('WIP - Verify full refund', async () => {
    const orderToken = await ScalaGeneralHelpers.generateCompleteOrder();
    console.log('New order token: ', orderToken);
    expect(orderToken).toBeTruthy();

    const paymentRefundEndpoint = `${configs.paymentsEndpoint}/${orderToken}/refund`;

    // Initiate order via API
    const testParams = {
      requestType: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `${configs.authScheme} ${configs.authToken}`
      },
      endpoint: paymentRefundEndpoint,
    };
    const data = JSON.stringify({
      refundAmount: {
        amount: '100.00',
        currency: 'EUR'
      },
      merchantReference: 'RF127261AD22'
    });

    const scalaResponse = await apiReq.scalaAPIRequest(testParams, data);
    console.log('Refund response', scalaResponse);
    expect(scalaResponse.status).toEqual(200);
  }, testTimeout);
});

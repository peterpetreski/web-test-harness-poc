/* eslint-disable no-await-in-loop */
/* eslint-disable no-undef */
const loginHelper = require('./utilities/login-helpers');
const envConfig = require('./env.config');

const testTimeout = 60000;
describe('Dashboard - Orders page tests', () => {
  beforeAll(async () => {
    // Add pre-test setup stuff here
    await loginHelper.gotoLoginAndSubmit();
    await page.waitForSelector('#userLoggedInData');
  }, testTimeout);

  test.only('Verify successful login and redirect to dashboard page', async () => {
    // verify we are on dashboard page
    await page.waitForSelector('#userLoggedInData');
    await expect(page.title()).resolves.toContain('Orders - Scalapay');
    const currentPage = page.url();
    expect(currentPage).toEqual(`${envConfig.rootURI}account/dashboard`);
  }, testTimeout);

  test('Verify dashboard page side menu items', async () => {
    await page.waitForSelector('.left-side-menu');
    await expect(page.title()).resolves.toContain('Orders - Scalapay');
    const sideBar = await page.$$eval('.left-side-menu', elems => elems.length);
    expect(sideBar).toEqual(1);

    const navItems = await page.$$('ul.metismenu > li > a');
    expect(navItems.length).toEqual(5);

    const menuItems = [];
    for (i = 0; i < navItems.length; i++) {
      // eslint-disable-next-line no-await-in-loop
      const itemText = await page.evaluate(body => body.innerText, navItems[i]);
      menuItems.push(itemText);
    }
    expect(menuItems).toEqual(expect.arrayContaining(['Orders', 'Payment schedule', 'In store', 'Account', 'Logout']));
  }, testTimeout);

  test('Verify amount due card exists in orders page', async () => {
    const actionAnchor = await page.$eval('a.action-icon', el => el.innerText);
    expect(actionAnchor).toMatch(/AMOUNT DUE/);
  }, testTimeout);

  test('Verify search elements on orders page', async () => {
    // search text field
    const searchInput = await page.$$eval('input#searchField', elems => elems.length);
    expect(searchInput).toEqual(1);

    // dropdown
    const changeRows = await page.$$eval('select#changeRows', elems => elems.length);
    expect(changeRows).toEqual(1);

    // status switch
    const orderStatus = await page.$$eval('input#orderStatusSwitch', elems => elems.length);
    expect(orderStatus).toEqual(1);
  }, testTimeout);

  test('Verify orders table exists with correct columns and data rows', async () => {
    await page.waitForSelector('table.gs-table');
    const ordersTableHandle = await page.$('table.gs-table');
    // verify 6 columns and correct names
    const thElems = await ordersTableHandle.$$('.gs-table-head > tr > th');
    expect(thElems.length).toEqual(6);

    const columnItems = [];
    for (i = 0; i < thElems.length; i++) {
      const colText = await page.evaluate(body => body.innerText, thElems[i]);
      columnItems.push(colText);
    }
    expect(columnItems).toEqual(expect.arrayContaining(['Order ID', 'Order date', 'Total amount', 'Amount due', 'Merchant', 'Actions']));

    // data row
    const trElems = await ordersTableHandle.$$('.gs-table-body > tr');
    const tdElems = await trElems[0].$$('td');
    expect(tdElems.length).toEqual(6);

    // verify Order ID data element is anchor with correct format href
    expect(await tdElems[0].$eval('a', elem => elem.getAttribute('href'))).toMatch(/\/account\/orders\/[A-Z0-9]*/);

    // verify Order date data elem date string
    const dateStr = await tdElems[1].evaluate(elem => elem.innerText);
    expect(typeof Date.parse(dateStr)).toBe('number');

    // verify Total amount data element is price
    const totalAmount = await tdElems[2].evaluate(elem => elem.innerText);
    expect(totalAmount).toMatch(/€[0-9]*.[0-9]{2}/);

    // verify Amount due data element is price
    const amountDue = await tdElems[3].evaluate(elem => elem.innerText);
    expect(amountDue).toMatch(/€[0-9]*.[0-9]{2}/);

    // verify Merchant data element is price
    const merchant = await tdElems[4].evaluate(elem => elem.innerText);
    expect(merchant).toEqual(expect.any(String));

    // verify last data element is button with correct name
    expect(await tdElems[5].$eval('button', elem => elem.innerText)).toEqual('Details');
  }, testTimeout);

  test.todo('Verify pagination elements');
});
